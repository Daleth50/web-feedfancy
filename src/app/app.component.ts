import { Component, OnInit } from '@angular/core';
import { Post } from './models';
import { PostService } from './post.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  posts: Post[];
  params = {
    'pageSize': 50,
  };

  constructor(private postSrv: PostService) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts(): void {
    this.postSrv.getPosts(this.params)
      .subscribe(posts => this.posts = posts.results);
  }
}
